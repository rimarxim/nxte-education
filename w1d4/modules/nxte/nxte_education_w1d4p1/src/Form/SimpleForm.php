<?php

namespace Drupal\nxte_education_w1d4p1\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the SimpleForm form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class SimpleForm extends FormBase {

  /**
   * Getter method for Form ID.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'nxte_education_w1d4p1_simple_form';
  }

  /**
   * Build the form.
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('This basic example shows a single text input element and a submit button'),
    ];

    $form['first_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('A'),
      '#description' => $this->t(''),
      '#required' => TRUE,
    ];

    $form['second_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('B'),
      '#description' => $this->t(''),
      '#required' => TRUE,
    ];

  $form['operation_select'] = [
    '#type' => 'select',
    '#title' => $this->t('Select operation'),
    '#options' => [
      '+' => $this->t('+'),
      '-' => $this->t('-'),
      '*' => $this->t('*'),
    ],
  ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }


    /**
   * Implements a form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $first_field  = $form_state->getValue('first_field');
    $second_field = $form_state->getValue('second_field');
    $operation_select = $form_state->getValue('operation_select');
    $result = 0;

    switch ( $operation_select ) {
      case '+':
        $result = $first_field + $second_field;
        break;
      case '-':
        $result = $first_field - $second_field;
        break;
      case '*':
        $result = $first_field * $second_field;
        break;
    }

    $this->messenger()->addMessage( 'Result: ' . $result );
  }


  /**
   * Implements form validation.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $first_field  = $form_state->getValue('first_field');
    $second_field = $form_state->getValue('second_field');

    if ( !is_numeric( $first_field ) ) {
      $form_state->setErrorByName('first_field', $this->t('Only integer input please'));
    }
    if ( !is_numeric( $second_field ) ) {
      $form_state->setErrorByName('second_field', $this->t('Only integer input please'));
    }

  }


}
