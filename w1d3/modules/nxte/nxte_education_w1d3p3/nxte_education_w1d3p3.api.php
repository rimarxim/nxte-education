<?php

/**
 * My custom hook
 * Return array of strings
 */
function hook_extra_node_keywords() {
  $stringsArray = array(
    'for',
    'ever',
  );
  return $stringsArray;
}