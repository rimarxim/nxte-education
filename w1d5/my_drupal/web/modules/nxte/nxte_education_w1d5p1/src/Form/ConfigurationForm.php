<?php

namespace Drupal\nxte_education_w1d5p1\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigurationForm.
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nxte_education_w1d5p1.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'configuration_form';
  }

  /**
   * Build Form
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nxte_education_w1d5p1.configuration');
    $form['value_a'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value A'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('value_a'),
    ];
    $form['value_b'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value B'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('value_b'),
    ];
    $form['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#options' => ['+' => $this->t('+'), '-' => $this->t('-'), '*' => $this->t('*')],
      '#size' => 1,
      '#default_value' => $config->get('operation'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements form validation.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $value_a  = $form_state->getValue('value_a');
    $value_b = $form_state->getValue('value_b');

    if ( !is_numeric( $value_a ) ) {
      $form_state->setErrorByName('value_a', $this->t('Only integer input please'));
    }
    if ( !is_numeric( $value_b ) ) {
      $form_state->setErrorByName('value_b', $this->t('Only integer input please'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('nxte_education_w1d5p1.configuration')
      ->set('value_a', $form_state->getValue('value_a'))
      ->set('value_b', $form_state->getValue('value_b'))
      ->set('operation', $form_state->getValue('operation'))
      ->save();
  }

}
