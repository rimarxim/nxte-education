<?php
namespace Drupal\nxte_education_w1d4p1\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An nxte_education_w1d4p1 controller.
 */
class ExampleController extends ControllerBase {

  /**
   * Returns an array for a test page.
   */
  public function content() {

    // Do something with your variables here.
    $myText = 'This is not just a default text!';
    $myNumber = 1;
    $myArray = [1, 2, 3];

    $mins = intval( date("i") );
    $seconds = $mins * 60;


    return [
      // Your theme hook name.
      '#theme' => 'nxte_education_w1d4p1_theme_hook',      
      // Your variables.
      '#variable1' => $myText,
      '#variable2' => $myNumber,
      '#variable3' => $myArray,
      '#variable4' => $seconds,
    ];



  }

}