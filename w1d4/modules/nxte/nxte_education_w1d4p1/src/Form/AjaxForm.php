<?php

namespace Drupal\nxte_education_w1d4p1\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the SimpleForm form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class AjaxForm extends FormBase {

  /**
   * Getter method for Form ID.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'nxte_education_w1d4p1_ajax_form';
  }

  /**
   * Build the form.
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    //
    $option_select_value = $form_state->getValue('option_select');

    $form['form_container'] = [
      '#type' => 'container',
      // Note that the ID here matches with the 'wrapper' value use for the
      // instrument family field's #ajax property.
      '#attributes' => ['id' => 'fieldset-container'],
    ];

    if ( !empty($option_select_value) && $option_select_value == '2' ) {
        $form['form_container']['first_field'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('A'),
          '#description' => $this->t(''),
          '#required' => FALSE,
        ];

        $form['form_container']['second_field'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('B'),
          '#description' => $this->t(''),
          '#required' => FALSE,
        ];
    }else{
         $form['form_container']['first_field'] = [
          '#type' => 'textfield',
          '#title' => $this->t('A'),
          '#description' => $this->t(''),
          '#required' => FALSE,
        ];

        $form['form_container']['second_field'] = [
          '#type' => 'textfield',
          '#title' => $this->t('B'),
          '#description' => $this->t(''),
          '#required' => FALSE,
        ]; 
    }


    $form['form_container']['option_select'] = [
        '#type' => 'select',
        '#title' => $this->t('Options'),
        '#description' => $this->t('Select option'),
        '#options' => [
          '1' => $this->t('Inputs'),
          '2' => $this->t('Сheckboxes'),
        ],
        // Bind an Ajax callback to the element.
        '#ajax' => [
        'event' => 'change',
        'wrapper' => 'fieldset-container',
        ],
    ];

    // Add a submit button that handles the submission of the form.
    $form['form_container']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#states' => [
        'visible' => ['body' => ['value' => TRUE]],
      ],
    ];

    return $form;
  }


    /**
   * Implements a form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state->setRebuild();

    /*$first_field  = $form_state->getValue('first_field');
    $second_field = $form_state->getValue('second_field');
    $operation_select = $form_state->getValue('operation_select');
    $result = 0;

    switch ( $operation_select ) {
      case '+':
        $result = $first_field + $second_field;
        break;
      case '-':
        $result = $first_field - $second_field;
        break;
      case '*':
        $result = $first_field * $second_field;
        break;
    }*/

    $this->messenger()->addMessage( 'Ajax Result' );
  }


  /**
   * Implements form validation.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    /*$first_field  = $form_state->getValue('first_field');
    $second_field = $form_state->getValue('second_field');

    if ( !is_numeric( $first_field ) ) {
      $form_state->setErrorByName('first_field', $this->t('Only integer input please'));
    }
    if ( !is_numeric( $second_field ) ) {
      $form_state->setErrorByName('second_field', $this->t('Only integer input please'));
    }*/

  }


}
