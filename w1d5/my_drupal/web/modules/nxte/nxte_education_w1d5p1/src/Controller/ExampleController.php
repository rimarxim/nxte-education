<?php
namespace Drupal\nxte_education_w1d5p1\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An nxte_education_w1d5p1 controller.
 */
class ExampleController extends ControllerBase {

  /**
   * Returns an array for a test page.
   */
  public function content() {

    $config           = \Drupal::config('nxte_education_w1d5p1.configuration');
    $value_a          = $config->get('value_a');
    $value_b          = $config->get('value_b');
    $operation_select = $config->get('operation');
    $result           = 0;

var_dump( $value_a, $value_b, $operation_select );

    switch ( $operation_select ) {
      case '+':
        $result = $value_a + $value_b;
        break;
      case '-':
        $result = $value_a - $value_b;
        break;
      case '*':
        $result = $value_a * $value_b;
        break;
    }

    return [
      // Your theme hook name.
      '#theme' => 'nxte_education_w1d5p1_theme_hook',      
      // Your variables.
      '#variable1' => $result,    
    ];

  }

}